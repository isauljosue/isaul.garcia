int data;
const int threshold = 400;

int BUTTONS = A0;
int SPEAKER = 5;
int MOTOR = 3;

void setup() {
  pinMode(SPEAKER, OUTPUT);
  pinMode(BUTTONS, INPUT);
  pinMode(MOTOR, OUTPUT);
  Serial.begin(9600);

}

void loop() {
 
  data = analogRead(BUTTONS);
  Serial.print("BUTTONPRESS = ");
  Serial.println(data);
  
   if (data > 1000) {
    digitalWrite(SPEAKER, LOW);
    digitalWrite(MOTOR, LOW);
  }  
    else if (data >= 600) {
    analogWrite(SPEAKER, 1500);
  }
    else if (data <= 600 and data > 200) 
  {
    analogWrite(MOTOR, 700);
  }
    else if (data <= 200) 
  {
    analogWrite(MOTOR, 1600);
  }
}
