int data;                               // the reading from the analog input

int BUTTONPRESS = A0;


void setup() {

  pinMode(BUTTONPRESS, INPUT); //initialize ldr sensor as INPUT
  Serial.begin(9600); //begin the serial monitor at 9600 baud

}

void loop() {

  data = analogRead(BUTTONPRESS);
  Serial.print("BUTTONS = ");
  Serial.println(data);
}
